'use strict';

module.exports = {
  extends: ['./lib/default.js', 'plugin:node/recommended'],
  parserOptions: {
    ecmaVersion: 2020,
  },
  env: {
    node: true,
  },
};
